package coverage_metrics;

/*
*
* This class provides example of SonarQube size metrics
*
*/

public class CoverageMetrics {

  public float f(int i) {
    int k = 0; /* default */
    if (i != 0) {
      k = 1;
    }
    return (float) i / (k + 1);
  }
//TODO

  /**
   * @param num
   */
  public static void findEvenOdd(int num) {
    // method body
    if (num % 2 == 0)
      System.out.println(num + " is even");
    else
      System.out.println(num + " is odd");
  }

  public static void findOnlyOdd(int num) {
    // method body
    if (num % 5 == 0)
      System.out.println(num + " is even");
  }
//TODO
  public static void sayHi(int num2) {
    if (num2 == 0) {
      System.out.println("hi"+num2);
    } else if (num2 == 1) {
      System.out.println("hi 2 "+num2);
    }
  }
}
